package hellopackage;
import java.util.Scanner;
import java.util.Random;
//imports all classes from the package
import java.util.*;
import secondpackage.Utilities;

public class Greeter {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rng = new Random();
        Utilities ult = new Utilities();

        System.out.println("Enter a number:");
        int num = sc.nextInt();

        num = ult.doubleMe(num);

        System.out.println(num);
    }
}